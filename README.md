# Implementation of the famous Libet experiment in Psychopy

Note, that this piece of work is based on the implementation by GitHub user *DimitriBr* with only slight adapatations. Original repository can be found here: https://github.com/DimitriBr/LibetClock
