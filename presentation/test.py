import json
import numpy

myStr = 'mh'
myInt = numpy.int64(0)
myInt = int(myInt)
myList =  [0.123133531523, 0.121461847619481, 0.196387634]
mydict = {'a': myStr, 'b': myInt, 'c': myList}

print(f'type of {myInt} is {type(myInt)}')

json_file = json.dumps(mydict)
filename = 'test.json'

with open(filename, 'w') as f:
    json.dump(json_file, f)