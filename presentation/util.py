from win32api import GetSystemMetrics, EnumDisplayDevices, EnumDisplaySettings
from psychopy import core
import time

def get_refresh():
    # Refresh Rate of the monitor
    device = EnumDisplayDevices()
    refresh = EnumDisplaySettings(device.DeviceName, -1).DisplayFrequency
    return refresh


def tcpChecker(tcpSocket):
    
    # If this value is received then quit experiment:
    val = 2
    abort = False

    start = time.time()
    if tcpSocket is not None:
        data = tcpSocket.recv(tcpSocket.BufferSize)
        data = tcpSocket.recv(tcpSocket.BufferSize)
        msg_int = int.from_bytes(data, "big")
        while msg_int == 0:
            print(f"Waiting for go signal, signal is currently {msg_int} ({data})")
            core.wait(0.05)
            data = tcpSocket.recv(tcpSocket.BufferSize)            
            msg_int = int.from_bytes(data, "big")

        end = time.time()
        print(f"Go signal is there: {msg_int}. Waited for {end-start:.2f} seconds")
        
        if msg_int == val:
            # Send confirmation that msg was received
            tcpSocket.send(int(val**2).to_bytes(1, byteorder="big"))
            print(f'received msg_int {msg_int}. Sending {val**2} as byte ({int(val**2).to_bytes(1, byteorder="big")})')
            abort = True 
        
    
    return abort    