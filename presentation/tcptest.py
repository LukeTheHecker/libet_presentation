import socket
from tcp import CustomSocket
 
TCP_IP = '192.168.2.128'  # '127.0.0.1'
TCP_PORT = 5005
BUFFER_SIZE = 1024
# MESSAGE = "Hello, World!"
 
s = CustomSocket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.BufferSize = BUFFER_SIZE
print(f'my s.BufferSize is {s.BufferSize} ')
while True:
    data = s.recv(BUFFER_SIZE)

    print(int.from_bytes(data, "big"))
    # print(data)