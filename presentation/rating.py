from psychopy import visual, core  # import some libraries from PsychoPy

#create a window
mywin = visual.Window([800,600], monitor="testMonitor", units="deg")

#create some stimuli

def rating_screen(win, text,precision=100, labels=['Trifft nicht zu', 'Trifft zu'], scale=None):
    ratingScale = visual.RatingScale(win, precision=precision, labels=labels, scale=scale)
    item = visual.TextStim(win, text=text)
    while ratingScale.noResponse:
        item.draw()
        ratingScale.draw()
        win.flip()
    rating = ratingScale.getRating()
    return rating

questions = ['Ging dem Knopfdruck ein innerer Drang/Impuls voraus?', 'Wie spontan kam es zum Knopfdruck?', 'Wie gut konnten Sie Ihre inneren Prozesse vor dem Knopfdruck wahrnehmen?']
ratings = []
for question in questions:
    rating = rating_screen(mywin, question)
    ratings.append(rating)
    print(rating)


#draw the stimuli and update the window
mywin.update()

#pause, so you get a chance to see it!
core.wait(2.0)