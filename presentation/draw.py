# from __future__ import annotations
from util import get_refresh, tcpChecker
from psychopy import visual, core, event
from math import radians, atan2, sin, cos, floor, ceil, acos, asin, degrees, sqrt
import random
import time

def draw_the_ball(ang, win, r, amount):
    v = (r-0.13*r) * cos(radians(90-ang*(360/amount)))
    w = (r-0.13*r) * sin(radians(90-ang*(360/amount)))
    ball = visual.Circle(win, radius = 0.024*r, edges = 100, pos = (v,w), fillColor = 'red', lineColor = 'red', units = 'pix')
    ball.draw()

def text_window(text_pr, win):
    event.clearEvents()
    while True:
        text_to_show = visual.TextStim(win, pos=(0, 0), text = text_pr, color = 'black', wrapWidth=1.5, antialias=True, units = 'norm', height = 0.06)
        text_to_show.draw()
        win.flip()
        press_to_start = event.getKeys()
        if len(press_to_start) > 0:
            break
        core.wait(0.25)

def draw_clock(win, r, correction=0):
    ''' This function draws the libet clock using Psychopy.
    Parameters:
    -----------
    win : obj, instance from psychopy.visual.Window() on which to draw the clock
    r : float, radius
    correction : [int, float], a value that can move the cyphers to left (neg) or  right (pos).'''

    circ = visual.Circle(win, radius=r, edges = 100,  units = 'pix')
    circ.setLineColor("black")
    circ.draw()
    for i in range (1, 61):
        x = r * cos (radians(90-6*i))
        y = r * sin(radians(90-6*i))
        if i % 5 == 0:
            v = (r-(1/14)*r) * cos(radians(90-6*i))
            w = (r-(1/14)*r) * sin(radians(90-6*i))
            c1 = (r + 0.085*r) * cos(radians(90 - 6*i))
            c2 = (r + 0.085*r) * sin(radians(90-6*i))
            cyph = visual.TextStim(win, pos = (c1+correction,c2), text= str(i), color = 'black',  units = 'pix' )
            cyph.draw()
        else:
            v = (r-(3/140)*r) * cos(radians(90-6*i))
            w = (r-(3/140)*r) * sin(radians(90-6*i))
        zaseka = visual.Line(win, start = (x,y), end = (v,w),  units = 'pix')
        zaseka.setLineColor('black')
        zaseka.draw()

    fixcir = visual.Circle(win, radius = 0.025*r, edges = 1000, color = 'black', units = 'pix')
    fixcir.draw()
    win.flip()
    core.wait(0.05)
    clockey = visual.BufferImageStim(win, buffer = 'front')

    return clockey

def draw_main(win, trial_number, r, trial_duration, isi, texts, waitBeforeClockhand=2, 
    tutorial=False, tcpCon=None, encoding='utf-8'):
    ''' Main Loop for the Libet Experiment 
    '''


    # Get refresh rate of monitor
    refresh = get_refresh()
    step = 1000 / refresh
    amount = trial_duration / step

    blank_white = visual.TextStim(win, pos=(0, 0), text = '', color = 'black', wrapWidth=1.5, antialias=True, units = 'norm', height = 0.06)
    
    abortkey = 'q'
    response_key = 'space'
    response = 'response'
    keytips = visual.TextStim(win, pos=(-800, -400), text = f'abort: {abortkey}', color = 'black')
    headline = visual.TextStim(win, pos=(0, 500), text = 'Tutorial Mode', color = 'black')
    annotations = [keytips, headline]
    clockey = draw_clock(win, r)


    # Definitions
    abort = False
    time_of_recall = [i for i in range(trial_number)]
    absolute_time_of_recall_s = [i for i in range(trial_number)]
    time_of_press_ms = [i for i in range(trial_number)]
    absolute_time_of_press_s = [i for i in range(trial_number)]
    real_duration_ms = [i for i in range(trial_number)]
    position_reported_in_pseudo_s = [i for i in range(trial_number)]
    report_to_real_ms = [i for i in range(trial_number)]
    difference_btw_reported_and_real_time_ms = [i for i in range(trial_number)]
    early_press = [0 for i in range(trial_number)]


    for trial in range(trial_number):
        #portEEG.setData(script_code) #trial start, pin with respect to script_code

        # Here we stop to see whether the experiment should continue: receive 1 through internal tcp Port
        # or whether it should stop: receive 0 through internal tcp port
        abort = tcpChecker(tcpCon)
        if abort:
            break

        waiting = core.StaticPeriod(screenHz = refresh)
        abs_timer = core.Clock()
        timer_press = core.Clock()
        rot_end = False
        to_rotate = True
        ang_fin = None
        i = 0
        shift = 0
        real_duration_ms[trial] = trial_duration
        # Start the rotation at random angle:
        start_ang = int(random.uniform(1, ceil(amount)))
        firstFrameInTrial = True
        while to_rotate == True:                
            timer_press.reset()
            for ang in range (start_ang, (ceil(amount) + 1)):

                if ang == ang_fin:
                    rot_end = True
                # Check Button Press
                press = event.getKeys(timeStamped=timer_press)
                hasresponse = any([response_key in pr for pr in press])
                if hasresponse and ang_fin is None:
                    if tcpCon is not None:
                        tcpCon.send(response.encode(encoding))
                    # print(f'registered keypress for response: {press}')
                    time_of_press_ms[trial] = press[0][1] * 1000
                    print(f'time_of_press_ms[trial] = {time_of_press_ms[trial]}')
                    absolute_time_of_press_s[trial] = abs_timer.getTime()
                    ang_fin = ang + int(random.uniform(ceil(500/step), ceil (800/step)))
                    if ang_fin > ceil(amount):
                        ang_fin = ang_fin - ceil(amount)
                        shift = 1
                    # print(f'time of press: {time_of_press_ms} ms')
                    # print(f'Angle: {ang_fin}')
                else:
                    if tcpCon is not None:
                        tcpCon.send(''.encode(encoding))
                # Draw next frame
                if rot_end == False:
                    waiting.start(step*0.001)
                    clockey.draw()
                    if tutorial:
                        [a.draw() for a in annotations]
                    
                    if firstFrameInTrial:
                        win.flip()
                        core.wait(waitBeforeClockhand)
                        firstFrameInTrial = False
                        continue
                    # else:
                    draw_the_ball(ang, win, r, amount)                    
                    win.flip()
                    waiting.complete()
                # Or Break out of loop
                elif rot_end == True:
                    to_rotate = False
                    break
                # Check if experiment aborted:
                hasabortkey = any([abortkey in pr for pr in press])
                if hasabortkey:
                    abort = True
                    break
            if abort:
                break
            if to_rotate == True and i != 0:
                real_duration_ms[trial] = timer_press.getTime() * 1000 
            i += 1
            # if to_rotate == True:
                # portEEG.setData(4+i%2) #12 o'clock 
            start_ang = 1
        if i == 1 or (i == 2 and shift == 1):
            early_press[trial] = 1
        if abort:
            break
        ### JUDGEMENT PHASE
        text_to_decide = visual.TextStim(win, text = texts['ask_clock_hand_text'], color = 'black', units = 'norm', wrapWidth=1.5, antialias=True)
        text_to_decide.draw()
        win.flip()
        event.clearEvents()
        core.wait(1.5)

        # portEEG.setData(8) #judgement clock onset
        timer_for_recall = core.Clock()
        clockey.draw()
        win.flip()
        event.clearEvents()
        mouse = event.Mouse(win = win)
        wasitclicked = mouse.getPressed()
        while wasitclicked[0] == False:
            x_pix, y_pix = mouse.getPos()
            wasitclicked = mouse.getPressed()
        # portEEG.setData(7)
        # print(f'trial {trial}, total number: {trial_number}')
        time_of_recall[trial] = timer_for_recall.getTime()
        absolute_time_of_recall_s[trial] = abs_timer.getTime()

        x = x_pix/r
        y = y_pix/r
        dist = sqrt(x**2 + y**2)
        angle = degrees(acos(x/dist))
        if y < 0:
            angle = 360-angle
        n = 90 - angle
        if n < 0:
            n = n + 360 
        position_reported_in_pseudo_s[trial] = (1/6) * n
        report_to_real_ms[trial] = (position_reported_in_pseudo_s[trial]/60) * real_duration_ms[trial] 
        difference_btw_reported_and_real_time_ms[trial] = report_to_real_ms[trial] - time_of_press_ms[trial]
        # print(f'difference_btw_reported_and_real_time_ms:{difference_btw_reported_and_real_time_ms}')

        if (trial + 1) < trial_number:
            # Show white screen 
            blank_white.draw()
            win.flip()
            core.wait(isi)            
        else:
            break
    
    result = {
        'early_press': early_press,
        'time_of_press_ms': time_of_press_ms,
        'absolute_time_of_press_s': absolute_time_of_press_s,
        'real_duration_ms': real_duration_ms,
        'position_reported_in_pseudo_s': position_reported_in_pseudo_s, 
        'report_to_real_ms': report_to_real_ms, 
        'difference_btw_reported_and_real_time_ms': difference_btw_reported_and_real_time_ms,
        'time_of_recall': time_of_recall,
        'absolute_time_of_recall_s': absolute_time_of_recall_s
    }
    return result