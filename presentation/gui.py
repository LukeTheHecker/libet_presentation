from numpy.lib.twodim_base import mask_indices
from psychopy import gui
from psychopy.logging import error

def get_gui():
    '''Create a GUI using psychopy with fixed elements of question. 
    Return:
    -------
    myDict : dict, dictionary with filled in parameters'''
    myDict = {
        'ID': '', 
        'Session': '01',
        'Language': 'DE'
    }
    missing_entries = True

    while missing_entries:
        myDlg = gui.DlgFromDict(dictionary=myDict, sortKeys=False, title="JWP's experiment", show=True)

        missing_entries = any([myDict[key] == '' for key in myDict.keys()])
        if missing_entries:
            myDlg = gui.Dlg(title="Some entry is missing!")
            myDlg.show()

    print(f'User input: {myDict}')

    return myDict

def get_instructions(myDict):
    if myDict['Language'].lower() == 'de':
        text_path = 'text/de/'
    elif myDict['Language'].lower() == 'en':
        text_path = 'text/en/'
    else:
        lingu = myDict['Language']
        print(f'language: {lingu}')
        raise Exception('The entered language is unavailable. Please choose either <en> or <de> ')

    ## Read texts
    texts = dict()
    # Welcome text
    f = open(text_path+'welcome.txt', "r")
    texts['welcome_text'] = f.read()
    # Press Space Bar
    f = open(text_path+'space_bar.txt', "r")
    texts['space_bar'] = f.read()
    # Instruction text
    f = open(text_path+'instruction.txt', "r")
    texts['instruction_text'] = f.read()
    # Ask clock hand
    f = open(text_path+'ask_clock_hand.txt', "r")
    texts['ask_clock_hand_text'] = f.read()
    # Goodbye text  
    f = open(text_path+'goodbye.txt', "r")
    texts['goodbye_text'] = f.read()
    # Get ready 
    f = open(text_path+'get_ready.txt', 'r')
    texts['get_ready_text'] = f.read()
    return texts
    
