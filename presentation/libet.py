from gui import get_gui, get_instructions
from draw import draw_the_ball, text_window, draw_clock, draw_main
from util import get_refresh
from psychopy import visual, core, monitors, clock, event, logging, parallel, gui
from psychopy.logging import error
import pyglet
from math import radians, atan2, sin, cos, floor, ceil, acos, asin, degrees, sqrt
import random
import pandas as pd 
import serial
from win32api import GetSystemMetrics, EnumDisplayDevices, EnumDisplaySettings
from socket import *
from tcp import CustomSocket
### GENERAL SETTINGS
logging.console.setLevel(logging.CRITICAL) #  for conveniece of use non-compatible Python versions. Note that without this setting, a lot of extra logs are present in console (meaningless ones)
pth_to_store = 'log/'


# Port settings
# portEEG = parallel.ParallelPort(address=16124) 
# portEEG = serial.Serial('/dev/tty.KeySerial1', timeout=None)  # serial port of a mac


# portEEG = serial.Serial('COM4', 19200, timeout=None)  # serial port of a PC



fullscr = False  # True
# Monitor Resolution
screen_width = GetSystemMetrics(0) #/ 2  # pix IF YOU DON'T WANT TO USE FULL SCREEN WINDOW
screen_height = GetSystemMetrics(1) #/ 2 # pix
r  = 0.34 * screen_height # Size of the clock

# Experiment Settings:
trial_number = 5000
isi = .5  # Default: 5
duration = 2550  # The overall duration
duration_upd = duration
waitBeforeClockhand = .2  # Default: 2
correction = 0 # 485  #  Correction of clock numbers

# TCP control from neurofeedback
# TCP_IP = '192.168.2.122'  # '127.0.0.1'
# TCP_PORT = 5005
# BufferSize = 1024
# encoding = 'utf-8'
# con = CustomSocket(AF_INET, SOCK_STREAM)
# con.connect((TCP_IP, TCP_PORT))
# con.BufferSize = BufferSize
# con.settimeout(None)
# con.state = 0  # con.recv(con.BufferSize)
# con.send(''.encode(encoding))
con = None
print("connected")

# Refresh Rate of the monitor
refresh = get_refresh()
step = 1000 / refresh  # Duration of a step of rotation, ms
amount = duration / step #Amount of red ball steps



# User Interface
myDict = get_gui()
texts = get_instructions(myDict)
datafile = open(f'{pth_to_store}{myDict["ID"]}.csv', "w") #creation of a log-file for PRESCREENING of data! All the Data is accumulated in .csv file (see the script bottom)

# Create Window to draw on
win = visual.Window(size = (screen_width, screen_height), color=(1,1,1), screen=1, units='pix',
    fullscr=fullscr, allowGUI=True)


# Intro & Instructions
text_window(texts['welcome_text'], win)
text_window(texts['instruction_text'], win)

## Libet Experiment
# Tutorial
# draw_main(win, 9999, r, duration, isi, texts, waitBeforeClockhand=waitBeforeClockhand, 
#     tutorial=True, tcpCon=None)
text_window(texts['get_ready_text'], win)
# Main Experiment
result = draw_main(win, trial_number, r, duration, isi, texts, 
    waitBeforeClockhand=waitBeforeClockhand, tutorial=False, tcpCon=con)

# Say Goodbye
text_window(texts['goodbye_text'], win)

### AFTER ALL
win.close()

## Store the results
if len(result) > 0:
    Data_Frame = pd.DataFrame({'Early press (before first reaching 12)': result['early_press'],
        'Time of a press (with respect to 12 o"clock), ms': result['time_of_press_ms'],
        'Time of press (absolute), s': result['absolute_time_of_press_s'],
        'Updated duration of a cycle, ms': result['real_duration_ms'],
        'Reported position (W), pseudosecons':  result['position_reported_in_pseudo_s'],
        'Reported position converted to time W (with respect to 12 o"oclock), ms': result['report_to_real_ms'],
        'Difference between W and real time of a press, ms': result['difference_btw_reported_and_real_time_ms'],
        'Time of recall (from onset of recall window), ms': result['time_of_recall'],
        'Time of recall (absolute), s': result['absolute_time_of_recall_s']
    })
    Data_Frame.to_csv(f'{pth_to_store}{myDict["ID"]}.csv')